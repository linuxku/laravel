# Laravel
Docker environment to run Laravel 

[![Pipeline Status](https://gitlab.com/linuxku/laravel/badges/main/pipeline.svg)](https://gitlab.com/linuxku/laravel/-/pipelines)
[![Latest Release](https://gitlab.com/linuxku/laravel/-/badges/release.svg)](https://gitlab.com/linuxku/laravel/-/releases)

[Source code](https://gitlab.com/linuxku/laravel.git)

## Requirements
* Docker version 18.06 or later
* Docker compose version 1.22 or later

