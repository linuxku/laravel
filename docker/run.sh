#!/bin/sh

cd /var/www/html

# php artisan migrate:fresh --seed
php artisan cache:clear
php artisan route:cache
php artisan config:cache
php artisan config:clear

chmod -R 777 storage

/usr/bin/supervisord -c /etc/supervisord.conf
